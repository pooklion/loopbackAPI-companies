module.exports = Benefit => {
    Benefit.byCompany = (companyId, cb) => {

        let ds = Benefit.dataSource;
        let sql = "select * from Benefit inner join CompanyBenefits on   CompanyBenefits.benefitId=Benefit._id where CompanyBenefits.companyId=?";

        ds.connector.query(sql, companyId, function (err, benefits) {

            if (err) console.error(err);

            cb(err, benefits);

        });

    };

    Benefit.remoteMethod(
        'byCompany',
        {
            http: { verb: 'get' },
            description: 'Get list of benefits by companyId',
            accepts: { arg: 'companyId', type: ['string'] },
            returns: { arg: 'data', type: ['Benefit'], root: true }
        }
    );

}
