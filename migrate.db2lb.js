/* eslint-disable no-underscore-dangle */
/* eslint-disable prettier/prettier */
/* eslint-disable camelcase */
/* eslint-disable strict */
/*
 * Discovers a table schema and outputs it into a file: run this script via:
 * $ node discover-schema.js
 */
//require('@babel/register')({ presets: ['@babel/env'], ignore: ['node_modules', '.next'] })
const path = require('path')
const fs = require('fs')
const app = require('./server/server.js')

const output_directory = path.resolve(__dirname, 'server', 'models')

function callback(err, schema) {
  if (err) {
    console.error(err)
    return
  }
  if (typeof schema !== 'object') {
    throw 'schema object not defined'
  }
  console.log(`Auto discovery for schema ${schema.name}`)
  /*
   * Convert schema name from CamelCase to dashed lowercase (loopback format
   * for json files describing models), for example: CamelCase -> camel-case.
   */
  // var model_name = schema.name.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
  const model_name = mapName(null, schema.name)

  console.log('Writing model JSON file..')
  // write model definition JSON file
  const now_ms = Date.now()
  const model_JSON_file = path.join(output_directory, `${model_name}.json`)
  // if JSON file exists
  if (fs.existsSync(model_JSON_file)) {
    // save a backup copy of the JSON file
    // const bkp_file = path.join(output_directory,`${model_name  }.json` + `.bkp_${  now_ms}`)
    // fs.renameSync(model_JSON_file, bkp_file)
    // console.log('Backing up old JSON file..')
  }
  // write the new JSON file
  /**
   *
   */
  const jsonFileContent = schema

  if (model_name === 'user') jsonFileContent.base = 'User'
  else jsonFileContent.base = 'PersistedModel'

  jsonFileContent.validations = []
  jsonFileContent.acls = []
  jsonFileContent.relations = {}
  jsonFileContent.methods = {}

  jsonFileContent.properties._id = { type: 'string', id: true, defaultFn: 'guid' }
  fs.writeFileSync(model_JSON_file, JSON.stringify(jsonFileContent, null, 2))
  console.log(`JSON saved to ${model_JSON_file}`)

  console.log('Writing model JS file..')
  // write model JS file, useful to extend a model with custom methods
  const model_JS_file = path.join(output_directory, `${model_name}.js`)
  // if JS file exists
  if (fs.existsSync(model_JS_file)) {
    // save a backup copy of the JS file
    // const bkp_file = path.join(output_directory,`${model_name  }.js` + `.bkp_${  now_ms}`)
    // fs.renameSync(model_JS_file, bkp_file)
    // console.log('Backing up old JS file..')
  }
  // write the new JS file
  fs.writeFileSync(
    model_JS_file,
    `module.exports = Model => {
 
}
`,
  )
  console.log(`JS saved to ${model_JSON_file}`)

  // Append model to model-config.json
  const model_config_file = path.resolve(__dirname, 'server', 'model-config.json')
  const model_config_obj = JSON.parse(fs.readFileSync(model_config_file, 'utf8'))
  if (typeof model_config_obj[model_name] === 'undefined') {
    const datasource = process.argv[3]
    model_config_obj[model_name] = {
      dataSource: 'db',
      public: true,
    }
    const json_content = JSON.stringify(model_config_obj, null, 2)
    fs.writeFileSync(model_config_file, JSON.stringify(model_config_obj, null, 2))
  }
}

function printUsage() {
  console.log(
    '\nUsage: node discover-schema.js [-ds datasource -sn db_schema_name]\n' +
      '\t-ds datasource: name of the datasource as specified in datasources.json\n' +
      '\t-tn db_table_name: name of the table in the db\n',
  )
}

// custom name mapper
function mapName(type, name) {
  return name
}

function main() {
  console.log(process.argv.length)
  // console.log(app)
  // console.log(Object.keys(app))
  // return false
  // return false
  switch (process.argv.length) {
    /*
     * if there are 6 params (first and second are execPath and JS file being executed)
     */
    case 4:
      // should be datasource

      // should be db schema name
      // var param21 = process.argv[4]
      var table_name = process.argv[3]

      var datasource_json = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'server', 'datasources.json'), 'utf8'))

      if (process.argv[2] === '-tn' && datasource_json.db) {
        const options = {}
        options.nameMapper = mapName
        const ds = app.datasources.db
        ds.discoverSchema(table_name, options, callback)
      } else {
        printUsage()
      }
      break
    default:
      printUsage()
  }
}

main()
